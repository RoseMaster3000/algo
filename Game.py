from copy import deepcopy
SIDE = 3
SIZE = SIDE ** 2
GOAL = [
    [1,2,3],
    [8,0,4],
    [7,6,5],
]

class Game:
    # create Game instance
    def __init__(self, start, swaps=0):
        self.swaps = swaps
        self.board = start
        self.children = []
        self.parent = None
        for i,row in enumerate(self.board):
            for j,cell in enumerate(row):
                if cell==0:
                    self.blank=(j,i)
                    return

    # make a copy of this Game object
    def clone(self):
        return Game(
            start=deepcopy(self.board),
            swaps=self.swaps,
        )

    # magic methods
    def __getitem__(self, i):
        x = i[0]
        y = i[1]
        return self.get(x, y)

    def __setitem__(self, i, value):
        x = i[0]
        y = i[1]
        return self.set(x, y, value)

    def __repr__(self):
        rows = [",".join([str(val) for val in row]) for row in self.board]
        return ";".join(rows)

    def __str__(self):
        rows = ["".join([str(val) for val in row]) for row in self.board]
        return "".join(rows)

    # fetch board value (arg:coord)
    def get(self, x, y):
        return self.board[y][x]

    # set board value (arg: coord, new value)
    def set(self, x, y, value):
        self.board[y][x] = value

    # return {list of tuples} representing swap-candidates coords
    def getOptions(self):
        coords = []
        for delta in [-1,+1]:
            x,y = self.blank
            xd = self.blank[0] + delta
            yd = self.blank[1] + delta
            if (xd in range(SIDE)):
                coords.append((xd, y))
            if (yd in range(SIDE)):
                coords.append((x, yd))
        return coords

    # swap blank piece with given coord
    def swap(self, coord):
        return self.swap(*coord)
    def swap(self, x, y):
        bx, by = self.blank
        self[bx,by] = self[x, y]
        self[x,y] = 0
        self.blank = (x, y)
        self.swaps += 1

    # have children projecting possible future board states
    def haveChildren(self):
        self.children = self.returnChildren()
    def returnChildren(self):
        children = []
        options = self.getOptions()
        for opt in options:
            child = self.clone()
            child.swap(*opt)
            child.parent = self
            children.append(child)
        return children

    # return int: of how many cells are solved
    def getScore(self):
        score = 0
        for i,row in enumerate(self.board):
            for j,cell in enumerate(row):
                if cell == GOAL[i][j]:
                    score += 1
        return score

    # return float: of how close this board is to solve
    # 1.0 == perfect / 0.0 == very bad
    def getPercent(self):
        return self.getScore() / SIZE

    # returns True if solved
    def isSolved(self):
        return (self.getScore() == SIZE)

    # print board (for debugging)
    def print(self):
        for i,row in enumerate(self.board):
            print("   ",end="")
            for col in row:
                if col==0:
                    print(" ", end="  ")
                else:
                    print(col, end="  ")
            if i==0:
                print(f"   blank: {self.blank}")
            elif i==1:
                print(f"   swaps: {self.swaps}")
            elif i==2:
                print(f"   score: {self.getScore()}")
            else:
                print()
        print()