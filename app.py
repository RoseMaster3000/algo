from Game import Game
from DepthFirst import dfs
from BreadthFirst import bfs
from AStar import ast
from UniformCost import ucs

#<x> \y/

easy = [
    [1,2,3],
    [0,6,4],
    [8,7,5],
]


medium = [
    [2,5,3],
    [0,1,4],
    [8,6,7],
]

impossible = [
    [1,2,3],
    [4,0,5],
    [6,7,8],
]


startGame = Game(medium)
startGame.print()
solvedGame = bfs(startGame)
print()
print("------------------")
if solvedGame==None:
    print("Game not solvable (odd number of inversions)")
while solvedGame!=None:
    solvedGame.print()
    solvedGame = solvedGame.parent
