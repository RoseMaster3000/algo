# Overview
A simple project which uses different search algorithms to solve "8 Puzzle."
It has implementations for the following:
* Depth-First Search (DFS)
* Uniform-Cost Search (UCS)
* Best-First Search (BFS)
* A Star (A∗)


### Setup
1. Clone this repo `git clone git@gitlab.com:RoseMaster3000/algo.git`
2. Run program `python app.py`


# Author
| Shahrose Kasim |             |
|----------------|-------------|
|*[shahros3@gmail.com](mailto:shahros3@gmail.com)*|[shahrose.com](http://shahrose.com)|
|*[rosemaster3000@gmail.com](mailto:rosemaster3000@gmail.com)*|[florasoft.live](https://florasoft.live) |
|*[RoseMaster#3000](https://discordapp.com/users/122224041296789508)*|[discord.com](https://discord.com/)|