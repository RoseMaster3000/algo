# Breadth First Search
#(search wide first)

from Utility import clearLine

# iterative (use our own stack + while loop)
def bfs(startGame):
    for _ in range(5): print()
    skipped = 0
    visited = 0
    bestScore = 0
    history = set()
    stack = [startGame]

    while (len(stack) > 0):
        # process current walk
        current = stack.pop(0)
        history.add(str(current))
        score = current.getScore()
        if score > bestScore:
            bestScore = score

        # check score (return if solved state found!)
        if score==9:
            return current

        # add future walks to tree
        for child in current.returnChildren():
            if str(child) in history:
                skipped += 1
            else:
                stack.append(child)

        # update terminal
        visited += 1
        for _ in range(5): clearLine()
        print(f"Visited Nodes: {visited:,}")
        print(f" Unique Nodes: {len(history):,}")
        print(f"Skipped Nodes: {skipped:,}")
        print(f"   Todo Nodes: {len(stack):,}")
        print(f"   Best Score: {bestScore}")


    # Tree exhasted (no solved states found)
    return None