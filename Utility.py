import sys

def clearLine(lineCount=1, width=None):
    for i in range(lineCount):
        sys.stdout.write("\033[K")
        sys.stdout.write("\033[F")
        if width!=None:
            if type(width)==str: width = len(width)
            print(" "*width)
            clearLine(lineCount=1, width=None)