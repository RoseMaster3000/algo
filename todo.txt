Depth-First Search (DFS)
Uniform-Cost Search (UCS)
Best-First Search (BFS)
A Star (A∗)
------------
count nodes visited
    (return tuple >> solution, visitedCount)
average run time
    https://docs.python.org/3/library/timeit.html
comments on algo
    (Readme.md)